<?php

use Phalcon\Text;
use Phalcon\Version;

echo Text::camelize(sprintf('hello from Phalcon %s with PHP %s :)', Version::get(), phpversion()));
